//
//  userCell.swift
//  Firebase App
//
//  Created by Asar Sunny on 30/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    //outlet
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var checkMarkImage: UIImageView!
    var showing = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        accessoryType = .checkmark
        if selected{
            if showing == false{
//                 accessoryView?.isHidden = false 
                checkMarkImage.isHidden = false
                showing = true
            }else{
                
//                accessoryView?.isHidden = true
                  checkMarkImage.isHidden = true
                showing = false
            }
           
           
            
        }
       
        // Configure the view for the selected state
    }
    
    
    
    func configureCell(withImage image:UIImage, andEmail email:String, isSelected:Bool){
        self.profileImage.image = image
        self.emailLabel.text = email
        if isSelected{
            checkMarkImage.isHidden = false
        }else{
            checkMarkImage.isHidden = true
        }
    }

}

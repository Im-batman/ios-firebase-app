//
//  DataService.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import Firebase



let DB_BASE = Database.database().reference()

class DataService{
    static let instance = DataService()
    
    // these are equivilent of urls, the are used to identify collections.
    private var _Ref_Base = DB_BASE
    private var _Ref_Users = DB_BASE.child("users")
    private var _Ref_Groups = DB_BASE.child("groups")
    private var _Ref_Feed = DB_BASE.child("feed")
    
    
    //accessors.
    var Ref_Base: DatabaseReference{
        return _Ref_Base
    }
    
    var Ref_Users: DatabaseReference{
        return _Ref_Users
    }
    
    var Ref_Groups:DatabaseReference{
        return _Ref_Groups
    }
    
    var Ref_Feed: DatabaseReference{
        return _Ref_Feed
    }
    
    
    //creating the user in database
    func createDbUser(uid:String, userData:Dictionary<String,Any>){
        Ref_Users.child(uid).updateChildValues(userData)
    }
    
    //making post
    func uploadPost(withMessage message:String, forUid uid:String, withGroupKey groupKey:String?,sendComplete:@escaping (_ status:Bool)->()){
        
        if groupKey != nil{
            //make a group post
        }else{
            Ref_Feed.childByAutoId().updateChildValues(["content":message,"senderId":uid])
            sendComplete(true)
            
        }
        
        
        
    }
    
    
    func fetchAllMessages(completion:@escaping (_ messages:[Message])->()){
        var messagesArray = [Message]()
        
        Ref_Feed.observeSingleEvent(of: .value) { (messageSnapShot) in
            guard let messageSnapShot = messageSnapShot.children.allObjects as? [DataSnapshot] else {return}
            
            for message in messageSnapShot{
                
                let content = message.childSnapshot(forPath: "content").value as! String
                let senderId = message.childSnapshot(forPath: "senderId").value as! String
                
                let newMessage = Message(senderId: senderId, content: content)
                messagesArray.append(newMessage)
                
            }
            completion(messagesArray)
            
        }
        
    }
    
    
    func getUserName(fromUid uid:String, completion:@escaping(_ username:String)->()){
        Ref_Users.observeSingleEvent(of: .value) { (userSnapshot) in
            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else {return}
            for user  in userSnapshot{
                if user.key == uid{
                    completion(user.childSnapshot(forPath: "email").value as! String)
                }
            }
        }
        
    }
    
    
    func getUserEmails(forSearchQuery query:String, completion:@escaping (_ emails:[String])->()){
        var emailsArray = [String]()
        
        Ref_Users.observe(.value) { (userSnapShot) in
            guard let userSnapShot = userSnapShot.children.allObjects as? [DataSnapshot] else {return}
            for user in userSnapShot{
                let email = user.childSnapshot(forPath: "email").value as! String
                if email.contains(query) == true && email != Auth.auth().currentUser?.email{
                    emailsArray.append(email)
                }
            }
        completion(emailsArray)
        }
    }
    
    
    func getUserIds(forUser username: [String], completion:@escaping (_ ids:[String])->()){
        Ref_Users.observeSingleEvent(of: .value) { (dataSnapShot) in
            guard let dataSnapShot = dataSnapShot.children.allObjects as? [DataSnapshot] else {return}
            var userIds = [String]()
            
            for user in dataSnapShot{
                if username.contains(user.childSnapshot(forPath: "email").value as! String){
                    userIds.append(user.key)
                }
            }
            completion(userIds)
            
        }
    }
    
    func createGroup(withtitle title:String, andDescription description:String, andMembers members: [String], completion:@escaping(_ done: Bool)-> ()){
        Ref_Groups.childByAutoId().updateChildValues(["title":title,"description":description,"members":members])
        completion(true)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

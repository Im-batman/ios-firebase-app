//
//  GroupCell.swift
//  Firebase App
//
//  Created by Asar Sunny on 03/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {

    //outlets
    
    @IBOutlet weak var groupTitle: UILabel!
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var groupMembers: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell(title:String, description:String,members:Int){
        groupTitle.text = title
        groupDescription.text = description
        groupMembers.text = "\(members) members"
    }
    
    
    
    
    

}

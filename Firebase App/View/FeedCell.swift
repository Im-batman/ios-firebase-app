//
//  FeedCell.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
//outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var message: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell(withImage image:UIImage,andUserID uid:String,andContent content:String){
        self.userImage.image = image
        self.message.text = content
        self.userName.text = uid
        
    }
    
    
    

}

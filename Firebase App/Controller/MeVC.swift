//
//  MeVC.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import Firebase

class MeVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //outlets
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailLabel.text = Auth.auth().currentUser?.email
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signOutBtnPressed(_ sender: Any) {
        
        let logOut = UIAlertController(title: "LogOut?", message: "Are You Sure You Want To LogOut?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let logOutAction = UIAlertAction(title: "LogOut", style: .destructive) { (action) in
            
            try? Auth.auth().signOut()
            let AuthVC = self.storyboard?.instantiateViewController(withIdentifier: "AuthVC")
            self.present(AuthVC!, animated: true, completion: nil)
        }
        
        logOut.addAction(logOutAction)
        present(logOut, animated: true, completion: nil)
        
        
//        
//        
//        
//        let imagePicker = UIImagePickerController()
//        imagePicker.delegate = self
//        
//        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
//            
//            let libraryAction = UIAlertAction(title: "library", style: .default, handler: { (action) in
//                imagePicker.allowsEditing = true
//                imagePicker.sourceType = .photoLibrary
//                self.present(imagePicker, animated: true, completion: nil)
//            })
//            
//            logOut.addAction(libraryAction)
//            
//            
//        }
        
        
        
        
        
        
        
        
    }
    
    
   
    
    
    
    
    


}

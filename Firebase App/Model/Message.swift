//
//  Message.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

class Message {
    
    private var _senderId:String
    private var _content:String
    
    
    
    var senderId: String{
        return _senderId
    }
    
    var content: String{
        return _content
    }
    
    init(senderId:String,content:String ) {
        self._senderId = senderId
        self._content = content
    }
    
    
}

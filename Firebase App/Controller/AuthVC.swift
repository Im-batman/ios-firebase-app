//
//  AuthVC.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import Firebase

class AuthVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil{
            dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func loginWithEmail(_ sender: UIButton) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        present(loginVC!, animated: true, completion: nil)
    }
    
    @IBAction func loginWithGoogle(_ sender: UIButton) {
    }
    
    @IBAction func loginWithFb(_ sender: Any) {
    }
    
    
    
    
    
    

}

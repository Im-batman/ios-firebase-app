//
//  LoginVC.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailField: InsetTextField!
    @IBOutlet weak var passwordField: InsetTextField!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        emailField.delegate = self
        passwordField.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signInWasPressed(_ sender: UIButton) {
        if emailField.text != nil && passwordField.text != nil {
            AuthService.instance.loginUser(withEmail: emailField.text!, andPassword: passwordField.text!, completion: { (success, loginError) in
                if success{
                    self.dismiss(animated: true, completion: nil)
                }else{
                    debugPrint(loginError?.localizedDescription)
                }
                AuthService.instance.registerUser(withEmail: self.emailField.text!, andPassword: self.passwordField.text!, completion: { (success, registerError) in
                    if success{
                        AuthService.instance.loginUser(withEmail: self.emailField.text!, andPassword: self.passwordField.text!, completion: { (success, error) in
                            self.dismiss(animated: true, completion: nil)
                        })
                    }else{
                        print("something happended again")
                    }
                    
                })
                
                
            })
            
        }
        
    }
    
    @IBAction func cloaseBtn(_ sender: Any) {
        print("close tapped")
        dismiss(animated: true, completion: nil)
    }
    
    
    
}



extension LoginVC: UITextFieldDelegate{
    
    
    
}

//
//  CreateGroupVC.swift
//  Firebase App
//
//  Created by Asar Sunny on 30/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import Firebase

class CreateGroupVC: UIViewController {
    
    var emailsArray = [String]()
    var chosenUserArray = [String]()
    
    
//outlet
    
    @IBOutlet weak var titleTxt: InsetTextField!
    @IBOutlet weak var descTxt: InsetTextField!
    @IBOutlet weak var emailSearch: InsetTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var membersLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        emailSearch.delegate = self
        emailSearch.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
    }
    
    
    
    
    @objc func textFieldChanged(){
        if emailSearch.text == ""{
            emailsArray.removeAll()
            tableView.reloadData()
        }else{
            
            DataService.instance.getUserEmails(forSearchQuery: emailSearch.text!, completion: { (returnedEmails) in
                self.emailsArray = returnedEmails
                self.tableView.reloadData()
            })
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func donePressed(_ sender: Any) {
        if titleTxt.text != "" && descTxt.text != ""{
            DataService.instance.getUserIds(forUser: chosenUserArray, completion: { (userIds) in
                var ids = userIds
                ids.append((Auth.auth().currentUser?.uid)!)
                DataService.instance.createGroup(withtitle: self.titleTxt.text!, andDescription: self.descTxt.text!, andMembers: ids, completion: { (completed) in
                    if completed{
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            })
        }
        
    }
    
    
    
    
    
    
}


extension CreateGroupVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as? UserCell else {return UITableViewCell()}
        let image = UIImage(named:"defaultProfileImage")!
        let email = emailsArray[indexPath.row]
        if chosenUserArray.contains(email){
            cell.configureCell(withImage: image, andEmail: email, isSelected: true)
        }else{
            cell.configureCell(withImage: image, andEmail: email, isSelected: false)
        }
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userEmail = emailsArray[indexPath.row]
        
        if !chosenUserArray.contains(userEmail){
            
            chosenUserArray.append(userEmail)
            membersLabel.text = chosenUserArray.joined(separator: ", ")
            doneBtn.isHidden = false
        }else{
            chosenUserArray = chosenUserArray.filter({$0 != userEmail})
            if chosenUserArray.count > 0{
                membersLabel.text = chosenUserArray.joined(separator: ", ")
            }else{
                membersLabel.text = "Group Member"
                doneBtn.isHidden = true
            }
            
            
        }
        
    }
    
    
    
    
    
}
extension CreateGroupVC: UITextFieldDelegate{
    
}

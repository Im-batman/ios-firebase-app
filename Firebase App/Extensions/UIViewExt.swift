//
//  UIViewExt.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit


extension UIView{
    
//    func bindToKeyboard(){
//
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name:Notification.Name.UIKeyboardWillChangeFrame, object: nil)
//
//    }
//
//
//
//    @objc func keyboardWillChange(_ notif:Notification){
//
//        let duration = notif.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
//        let curve = notif.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
//
//        let startingPoint = (notif.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
//        let endingPoint = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        let deltaY = endingPoint.origin.y - startingPoint.origin.y
//
//        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue:curve), animations: {
//            self.frame.origin.y += deltaY
//        }, completion: nil)
//
//    }

    
    
    
    
    
    
    
    
    func bindToKeyboard(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    
    
    @objc func keyboardWillChangeFrame(_ notfi:Notification){
        
        let duration = notfi.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notfi.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        
        let startingPoint = (notfi.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let endingPoint = (notfi.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        let deltaY = endingPoint.origin.y - startingPoint.origin.y
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue:curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: nil)
        
    }
    
    
    
    
    
    
    
    
    
}

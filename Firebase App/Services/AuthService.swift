//
//  AuthService.swift
//  Firebase App
//
//  Created by Asar Sunny on 29/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import Firebase

class AuthService{
    static let instance = AuthService()
   
    
    
    
    
    func registerUser(withEmail email:String, andPassword password:String, completion:@escaping (_ status: Bool, _ error: Error?)->()){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else { completion(false,error)
                return}

            let userData = [
                "provider": user.user.providerID,
                "email": user.user.email
            ]
            DataService.instance.createDbUser(uid: user.user.uid, userData: userData)
            completion(true, nil)
            
        }
        
        
    }
    
    
    func loginUser(withEmail email:String, andPassword password:String, completion:@escaping (_ status: Bool, _ error: Error?)->()){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
//            guard let user = user else { completion(false,error)
//                return
//            }

            if error != nil{
                completion(false, error)
                return
            }
            completion(true, nil)
        }
        
    }
    
    
    
    
    
    
    
}
